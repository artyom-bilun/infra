provider "aws" {
  version = "~> 2.0"
  region  = var.aws_region
}

resource "aws_s3_bucket" "tf_state" {
  bucket = var.s3_bucket_name
  region = var.aws_region

  versioning {
    enabled = true
  }
#   lifecycle {
#     prevent_destroy = true
#   }


}
