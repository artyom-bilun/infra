variable "aws_region" {
  description = "AWS region"
  default     = "us-east-1"
}
variable "s3_bucket_name" {
  description = "S3 bucket name"
  default     = "totally-not-a-tf-state-s3-bucket"
}
