#cloud-config
packages:
  - docker
runcmd:
  - systemctl enable docker
  - systemctl start docker
  - docker run -p 8000:8000 -d --name app ${app_docker_image}
