variable "app_docker_image" {
  default = "registry.gitlab.com/artyom-bilun/devops-challenge:latest"
}
variable "aws_region" {
  description = "AWS region"
  default     = "us-east-1"
}
