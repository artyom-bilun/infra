# Infra

Use `backend-init` tf directory to initialize your backend.
`cloud-init` directory contains user-data files passeed into EC2 instances.

What could be improved:
- Use private subnets for backend (NAT gateway is outside of Free-tier scope)
- Use autoscaling group with UpdatePolicy assigned and a loadbalancer behind
- An entier app could be replaced with a single lambda function
- Make code modular
- Support multiple environments
